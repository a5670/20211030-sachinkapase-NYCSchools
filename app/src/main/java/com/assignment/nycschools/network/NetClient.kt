package com.assignment.nycschools.network

import com.google.gson.GsonBuilder
import io.reactivex.rxjava3.schedulers.Schedulers
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava3.RxJava3CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

/**
 *  This is network class to make connection with server with Retrofit library.
 */
object NetClient {

    private const val BASE_URL: String = "https://data.cityofnewyork.us/resource/"
    private const val timeout = 60L
    private var retrofit: Retrofit? = null

    /**
     * This method is used to get the retrofit client.
     * @return [Retrofit] object
     */
       private fun createClient() {
            val builder = Retrofit.Builder()
            builder.addConverterFactory(GsonConverterFactory.create(GsonBuilder().create()))
            builder.baseUrl(BASE_URL)
            builder.addCallAdapterFactory(RxJava3CallAdapterFactory.createWithScheduler(Schedulers.io()))
            val okHttpClient = OkHttpClient.Builder()
                .connectTimeout(timeout, TimeUnit.SECONDS)
                .writeTimeout(timeout, TimeUnit.SECONDS)
                .addInterceptor(RequestInterceptor())
                .addInterceptor(HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY))
                .build()
            builder.client(okHttpClient)
            retrofit = builder.build()

        }

    fun <S> createService(serviceClass: Class<S>?): S {
        if(retrofit == null){
            createClient()
        }
        return retrofit!!.create(serviceClass)
    }
}