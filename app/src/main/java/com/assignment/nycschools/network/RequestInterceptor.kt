package com.assignment.nycschools.network

import android.util.Log
import okhttp3.Interceptor
import okhttp3.Response

/**
 *  This is request interceptor used to update the request headers of server requests.
 */
class RequestInterceptor : Interceptor {

    private val appToken = "UFcig4m6KXnMwaJAztCGTPX5H"

    override fun intercept(chain: Interceptor.Chain): Response {
        var request =  chain.request().newBuilder()
            .header("X-App-Token", appToken)
            .build()
        Log.i("URL", request.url.toString())
       return chain.proceed(request)
    }
}