package com.assignment.nycschools.util

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.telephony.PhoneNumberUtils

/**
 *  This phone util class to make phone calls from the application.
 */
class Phone {

    companion object {
        fun makeCall(context: Context, phone_number: String){
            val intent = Intent(Intent.ACTION_CALL)
            var phoneNumber = PhoneNumberUtils.formatNumber(phone_number, "US")
            intent.data = Uri.parse("tel:$phoneNumber")
            context.startActivity(intent)
        }
    }
}