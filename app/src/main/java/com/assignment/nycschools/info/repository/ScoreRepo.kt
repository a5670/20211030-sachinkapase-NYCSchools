package com.assignment.nycschools.info.repository

import androidx.lifecycle.MutableLiveData
import com.assignment.nycschools.info.dto.ScoresDetailDTO
import com.assignment.nycschools.info.InfoAPIService
import com.assignment.nycschools.network.NetClient
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.core.Observer
import io.reactivex.rxjava3.disposables.Disposable
import io.reactivex.rxjava3.schedulers.Schedulers

/**
 *  This repository class is used to get school information
 */
class ScoreRepo {


    companion object {
        private var scoreRepo: ScoreRepo? = null
        fun getInstance(): ScoreRepo? {
            if (scoreRepo == null) {
                scoreRepo = ScoreRepo()
            }
            return scoreRepo
        }
    }

    private var infoAPIService: InfoAPIService? = null


    /**
     *  This method is used to get the score details for selected school.
     *  @param dbn: String District Borough Number
     */
    fun getScoreDetailsForSchool() : MutableLiveData<List<ScoresDetailDTO>> {

        val scoreDataForSchool: MutableLiveData<List<ScoresDetailDTO>> = MutableLiveData<List<ScoresDetailDTO>>()
        infoAPIService = NetClient.createService(InfoAPIService::class.java)
        infoAPIService!!.getScoreDetailsForSchool().subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : Observer<List<ScoresDetailDTO>> {
                override fun onNext(t: List<ScoresDetailDTO>) {
                    scoreDataForSchool.value = t
                }
                override fun onError(e: Throwable) {
                    // Handle Error
                }
                override fun onComplete() {
                    // do nothing
                }
                override fun onSubscribe(d: Disposable) {
                }
            })
        return scoreDataForSchool
    }

}
