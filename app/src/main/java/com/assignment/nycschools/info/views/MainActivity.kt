package com.assignment.nycschools.info.views

import android.Manifest
import android.content.pm.PackageManager
import android.os.Bundle
import android.view.View
import android.widget.ProgressBar
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.SearchView
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.assignment.nycschools.R
import com.assignment.nycschools.info.dto.SchoolDetailsDTO
import com.assignment.nycschools.info.viewmodels.SchoolsModel
import com.assignment.nycschools.info.views.adapters.SchoolListItemAdapter
import com.assignment.nycschools.util.Phone


class MainActivity : AppCompatActivity(), SchoolListItemAdapter.OnSchoolSelectionListener{

    private lateinit var searchView: SearchView
    private var schoolModel : SchoolsModel = ViewModelProvider.NewInstanceFactory().create(SchoolsModel::class.java)
    private var recyclerView: RecyclerView? = null
    private var itemListAdapter: SchoolListItemAdapter? = null
    private var progressBar: ProgressBar? = null
    private var callNumber: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        searchView = findViewById<SearchView>(R.id.searchView)
        recyclerView = findViewById(R.id.rv_schools_list)

        progressBar = findViewById<ProgressBar>(R.id.progress_circular)
        progressBar!!.visibility = View.VISIBLE
        schoolModel.init()
        schoolModel.getSchoolsData()!!.observe(this,  Observer {
            recyclerView!!.layoutManager = LinearLayoutManager(this)
            itemListAdapter = SchoolListItemAdapter(this, it!!.toMutableList(), this)
            recyclerView!!.adapter = itemListAdapter
            progressBar!!.visibility = View.GONE
        })

        // Attach query listener to search the input value in search view
        searchView.setOnQueryTextListener(
            object : SearchView.OnQueryTextListener {
                // Override onQueryTextSubmit method  which is call when submit query is searched.
                // This will execute when user press search icon on keyboard.
                override fun onQueryTextSubmit(query: String): Boolean {
                    var filteredList = schoolModel.filter(query)
                    updateList(filteredList!!)
                    return false
                }

                // This method is overridden to filter the adapter according to a search query when the user is typing search
                override fun onQueryTextChange(newText: String): Boolean {
                    if(newText.isNullOrEmpty()){
                        // do nothing
                    } else {
                        var filteredList = schoolModel.filter(newText)
                        updateList(filteredList!!)
                    }
                    return false
                }
            })
    }

    /**
     *  This method is used to update the list in adapter based on filter result.
     */
    private fun updateList(filteredList : List<SchoolDetailsDTO>){
        if(filteredList.isEmpty()){
            Toast.makeText(this, "No matching data with given text.", Toast.LENGTH_SHORT).show()
        } else {
            itemListAdapter!!.updateSchoolData(filteredList!!.toMutableList())
        }
    }

    /**
     *  This method is used handle the user action on list of schools. It will navigate to
     *  school details activity.
     */
    override fun onSchoolSelected(schoolDetailsDTO: SchoolDetailsDTO) {
        SchoolDetailsActivity.launchActivity(this, schoolDetailsDTO)
    }

    override fun onPhoneNumberClicked(phoneNumber: String) {
        callNumber = phoneNumber
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.CALL_PHONE)  != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(
                this, arrayOf(Manifest.permission.CALL_PHONE),
                1001)
        } else {
            Phone.makeCall(this, callNumber!!)
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int,
                                            permissions: Array<String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            1001 -> {
                if ((grantResults.isNotEmpty() &&
                            grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                    Phone.makeCall(this, callNumber!!)
                } else {
                    // do nothing
                }
                return
            }
            else -> {
             // do nothing
            }
        }
    }
}