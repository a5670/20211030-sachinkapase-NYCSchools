package com.assignment.nycschools.info.dto

import java.io.Serializable

/**
 *  This is data object class for schools detail information.
 */
class SchoolDetailsDTO : Serializable {
    var dbn: String? = null
    var school_name: String? = null
    var location: String? = null
    var phone_number: String? = null
    var fax_number: String? = null
    var school_email: String? = null
    var website: String? = null
    var primary_address_line_1: String? = null
    var city: String? = null
    var zip: String? = null
    var state_code: String? = null
    var latitude: String? = null
    var longitude: String? = null
}

/**
"dbn":"27Q314",
"school_name":"Epic High School - South",
"boro":"Q",
"overview_paragraph":"Epic High School Â– South, an outgrowth of the NYC Expanded Success Initiative (ESI), provides a rigorous, culturally relevant academic program that prepares students for the demands of college and careers. EpicÂ’s personalized approach to instruction challenges students to dream big and design their futures. It engages students in solving real-world problems and supports their individual progress and growth. Epic students graduate with confidence in their ability to transform the world around them.",
"academicopportunities1":"Culturally relevant practices; Blended instruction with both teacher-led classes and use of digital resources",
"academicopportunities2":"Real-world problem-based learning, Student choice on elective courses; CORE advisory program focused on personal and social development",
"academicopportunities3":"Mentoring; Rites of passage experiences; College and career counseling starting in ninth grade; Internships; College-level courses in upper grades",
"ell_programs":"English as a New Language",
"neighborhood":"South Ozone Park",
"shared_space":"Yes",
"building_code":"Q226",
"location":"121-10 Rockaway Boulevard, South Ozone Park NY 11420 (40.675021, -73.81673)",
"phone_number":"718-845-1290",
"fax_number":"718-843-2072",
"school_email":"info@epicschoolsnyc.org",
"website":"www.epicschoolsnyc.org",
"subway":"N/A",
"bus":"Q10, Q37, Q7, Q9, QM18",
"grades2018":"9-12",
"finalgrades":"9-12",
"total_students":"175",
"start_time":"9am",
"end_time":"4pm",
"addtl_info1":"Internships",
"extracurricular_activities":"Based on student interest",
"attendance_rate":"0.879999995",
"pct_stu_enough_variety":"0.540000021",
"pct_stu_safe":"0.870000005",
"school_accessibility_description":"1",
"offer_rate1":"Â—14% of offers went to this group",
"program1":"Epic High School - South",
"code1":"Q66A",
"interest1":"Humanities & Interdisciplinary",
"method1":"Limited Unscreened",
"seats9ge1":"69",
"grade9gefilledflag1":"N",
"grade9geapplicants1":"194",
"seats9swd1":"12",
"grade9swdfilledflag1":"N",
"grade9swdapplicants1":"46",
"seats101":"No",
"admissionspriority11":"Priority to Queens students or residents who attend an information session",
"admissionspriority21":"Then to New York City residents who attend an information session",
"admissionspriority31":"Then to Queens students or residents",
"admissionspriority41":"Then to New York City residents",
"grade9geapplicantsperseat1":"3",
"grade9swdapplicantsperseat1":"4",
"primary_address_line_1":"121-10 Rockaway Boulevard",
"city":"South Ozone Park",
"zip":"11420",
"state_code":"NY",
"latitude":"40.67502",
"longitude":"-73.8167",
"community_board":"10",
"council_district":"28",
"census_tract":"840",
"bin":"4253607",
"bbl":"4117140100",
"nta":"South Ozone Park ",
"borough":"QUEENS
 */