package com.assignment.nycschools.info.views.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.assignment.nycschools.R
import com.assignment.nycschools.databinding.SchoolListItemBinding
import com.assignment.nycschools.info.dto.SchoolDetailsDTO

class SchoolListItemAdapter(var context: Context,
                            var schoolsList: MutableList<SchoolDetailsDTO>,
                            var onItemClickListener: OnSchoolSelectionListener)
    : RecyclerView.Adapter<SchoolListItemAdapter.SchoolListItemViewHolder?>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int):  SchoolListItemViewHolder {
        var schoolListItemBinding : SchoolListItemBinding =  DataBindingUtil.inflate(
            LayoutInflater.from(context), R.layout.school_list_item, parent, false)
        return SchoolListItemViewHolder( schoolListItemBinding)
    }

    override fun onBindViewHolder(holder: SchoolListItemViewHolder, position: Int) {
        var schoolModelResponseDTO = schoolsList[position]
        holder.itemBinding.schoolData = schoolModelResponseDTO
        holder.itemBinding.listener = onItemClickListener
    }

    override fun getItemCount(): Int {
        return schoolsList!!.size
    }

    fun updateSchoolData(updatedSchoolsList: MutableList<SchoolDetailsDTO>){
        schoolsList = updatedSchoolsList
        notifyDataSetChanged()
    }

    class SchoolListItemViewHolder(
        schoolListItemBinding: SchoolListItemBinding)
        : RecyclerView.ViewHolder(schoolListItemBinding.root) {
        var itemBinding = schoolListItemBinding
    }

    interface OnSchoolSelectionListener {
        fun onSchoolSelected(
            schoolDetailsDTO: SchoolDetailsDTO)

        fun onPhoneNumberClicked(phoneNumber: String)
    }


}