package com.assignment.nycschools.info

import com.assignment.nycschools.info.dto.SchoolDetailsDTO
import com.assignment.nycschools.info.dto.ScoresDetailDTO
import io.reactivex.rxjava3.core.Observable
import retrofit2.http.GET


interface InfoAPIService {

    @GET("s3k6-pzi2.json")
    fun getSchoolsList() : Observable<List<SchoolDetailsDTO>>

    @GET("f9bf-2cp4.json")
    fun getScoreDetailsForSchool() : Observable<List<ScoresDetailDTO>>

}