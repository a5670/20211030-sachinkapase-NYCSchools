package com.assignment.nycschools.info.viewmodels

import androidx.lifecycle.ViewModel
import androidx.lifecycle.LiveData

import androidx.lifecycle.MutableLiveData
import com.assignment.nycschools.info.dto.SchoolDetailsDTO
import com.assignment.nycschools.info.repository.SchoolRepo


class SchoolsModel : ViewModel(){

    private var schoolsList: MutableLiveData<List<SchoolDetailsDTO>>? = null
    private var schoolRepo: SchoolRepo? = null


    fun init() {
        if (schoolsList != null) {
            return
        }
        schoolRepo = SchoolRepo.getInstance()
        schoolsList = schoolRepo!!.getSchoolsData()
    }

    /**
     *  This method returns the schools list loaded from repository
     */
    fun getSchoolsData(): LiveData<List<SchoolDetailsDTO>>? {
        return schoolsList
    }

    /**
     * This method is used to filter the schools list based on user
     * input to search the school with name or city.
     */
    fun filter(query: String): List<SchoolDetailsDTO>? {
        return schoolsList!!.value!!.filter {
            it.school_name!!.startsWith(query, ignoreCase = true)
                    || it.city!!.startsWith(query, ignoreCase = true)
        }
    }

}