package com.assignment.nycschools.info.viewmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.assignment.nycschools.info.dto.ScoresDetailDTO
import com.assignment.nycschools.info.repository.ScoreRepo


/**
 * This class is used as View Model class for Scores information.
 * It will load the scores information from server and notify UI to display it.
 */
class ScoresModel : ViewModel(){

    private var scoreRepo: ScoreRepo? = null
    private var mutableLiveData: MutableLiveData<List<ScoresDetailDTO>>? = null

    /**
     *  This method is used to get the scores for all schools.
     *  The api for getting the score for single school was returning empty
     *  data so we are loading all schools data and then will filter based
     *  on selected school
     */
    fun getScoresForAllSchools(): LiveData<List<ScoresDetailDTO>>? {
        if(mutableLiveData == null) {
            scoreRepo = ScoreRepo.getInstance()
            mutableLiveData = scoreRepo!!.getScoreDetailsForSchool()
        }
        return mutableLiveData
    }

    /**
     * This method is sued to get the scores for selected school with its
     * dbn number.
     */
    fun getScoresForSchool(dbn: String) : List<ScoresDetailDTO>{
        return mutableLiveData!!.value!!.filter {
            it.dbn.equals(dbn)
        }
    }

}