package com.assignment.nycschools.info.views

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.ProgressBar
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.assignment.nycschools.R
import com.assignment.nycschools.databinding.ActivitySchoolDetailsBinding
import com.assignment.nycschools.info.dto.SchoolDetailsDTO
import com.assignment.nycschools.info.viewmodels.ScoresModel

class SchoolDetailsActivity : AppCompatActivity() {

    private var activitySchoolDetailsBinding : ActivitySchoolDetailsBinding? = null
    private var scoresModel : ScoresModel = ViewModelProvider.NewInstanceFactory().create(
        ScoresModel::class.java)
    private var schoolDetailsDTO : SchoolDetailsDTO? = null
    private var schoolDBN  = ""
    private var progressBar: ProgressBar? = null
    private var callNumber: String? = null

    companion object {
        private const val INTENT_SCHOOL_OBJECT  = "INTENT_SCHOOL_OBJECT"
        fun launchActivity(context : Context, schoolDetailsDTO: SchoolDetailsDTO) {
            var intent = Intent(context, SchoolDetailsActivity::class.java)
            intent.putExtra(INTENT_SCHOOL_OBJECT, schoolDetailsDTO)
            context.startActivity(intent)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        activitySchoolDetailsBinding = DataBindingUtil.setContentView(
            this, R.layout.activity_school_details
        )
        if (intent.hasExtra(INTENT_SCHOOL_OBJECT)) {
            schoolDetailsDTO = intent.getSerializableExtra(INTENT_SCHOOL_OBJECT) as SchoolDetailsDTO
            activitySchoolDetailsBinding!!.schoolDetailsDTO = schoolDetailsDTO
            schoolDBN = schoolDetailsDTO!!.dbn!!
        }
        progressBar = findViewById<ProgressBar>(R.id.progress_circular)
        progressBar!!.visibility = View.VISIBLE

        scoresModel.getScoresForAllSchools()!!.observe(this, Observer {
            if (it == null) {

            } else {
                activitySchoolDetailsBinding!!.scoreDetailsDTO =
                    scoresModel.getScoresForSchool(schoolDBN)[0]
                progressBar!!.visibility = View.GONE
            }
        })

    }
}