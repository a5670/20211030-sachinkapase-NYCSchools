package com.assignment.nycschools.info.dto

import java.io.Serializable

/**
 *  This is data object class to store the parsed data of schools scores
 */
class ScoresDetailDTO : Serializable{
    /*[{"dbn":"21K344",
    "school_name":"RACHEL CARSON HIGH SCHOOL FOR COASTAL STUDIES",
    "num_of_sat_test_takers":"54",
    "sat_critical_reading_avg_score":"402","
    sat_math_avg_score":"427",
    "sat_writing_avg_score":"408"
    }]*/
    var dbn : String? = null
    var school_name : String? = null
    var num_of_sat_test_takers : String? = null
    var sat_critical_reading_avg_score : String? = null
    var sat_math_avg_score : String? = null
    var sat_writing_avg_score : String? = null
}


