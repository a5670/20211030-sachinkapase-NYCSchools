package com.assignment.nycschools.info.repository

import androidx.lifecycle.MutableLiveData
import com.assignment.nycschools.info.dto.SchoolDetailsDTO
import com.assignment.nycschools.info.InfoAPIService
import com.assignment.nycschools.network.NetClient
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.core.Observer
import io.reactivex.rxjava3.disposables.Disposable
import io.reactivex.rxjava3.schedulers.Schedulers


/**
 *  This repository class is used to get school information
 */
class SchoolRepo {

    companion object {
        private var schoolRepo: SchoolRepo? = null
        fun getInstance(): SchoolRepo? {
            if (schoolRepo == null) {
                schoolRepo = SchoolRepo()
            }
            return schoolRepo
        }
    }

    private var infoAPIService: InfoAPIService? = null

    /**
     *  This method is used to get the list of schools and send back to model.
     */
     fun getSchoolsData(): MutableLiveData<List<SchoolDetailsDTO>> {
        val schoolData: MutableLiveData<List<SchoolDetailsDTO>> =
            MutableLiveData<List<SchoolDetailsDTO>>()
            infoAPIService = NetClient.createService(InfoAPIService::class.java)
            infoAPIService!!.getSchoolsList()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : Observer<List<SchoolDetailsDTO>> {
                override fun onNext(t: List<SchoolDetailsDTO>?) {
                    schoolData.value = t
                }
                override fun onError(e: Throwable) {
                    // Handle Error
                }
                override fun onComplete() {
                    // do nothing
                }
                override fun onSubscribe(d: Disposable) {
                }
            })
        return schoolData
    }
}
